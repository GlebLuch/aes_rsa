package main;

import cryptography.AES;
import cryptography.RSA;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        System.out.println("Введите сообщение: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        SecretKey key = AES.generateKey(128);
        IvParameterSpec ivParameterSpec = AES.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";
        String cipherTextAES = AES.encrypt(algorithm, input, key, ivParameterSpec);
        String decipherTextAES = AES.decrypt(algorithm, cipherTextAES, key, ivParameterSpec);
        System.out.println("AES: ");
        System.out.println(cipherTextAES);
        System.out.println(decipherTextAES);

        System.out.println("RSA: ");
        KeyPair keyPair = RSA.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();
        String cipherTextRSA = RSA.encrypt(input, publicKey);
        String decipherTextRSA = RSA.decrypt(cipherTextRSA, privateKey);
        System.out.println(cipherTextRSA);
        System.out.println(decipherTextRSA);
    }
}
